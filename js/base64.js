var btoa = require('btoa');

var atob = require('atob');

function base64_encode(buf)	{

	//Input:buf is a bytes array object
	
	//Convert it to and return the base64-encoded string object

	buf = Buffer.alloc(0);

	return buf.toString('utf-8');
	
//	return btoa(buf);
}

function base64_decode(s)	{
	
	//Input:s is a string object
	
	//Convert it to and return the corresponding raw bytes array object buf

	s = "";

	return Buffer.from(s,'utf-8');
	
//	return atob(s);
}

var str = process.argv[2];

bufferText = Buffer.from(str,'base64');

encodedStr = base64_encode(bufferText);

decodedStr = base64_decode(encodedStr);

process.stdout.write("raw input:0x");

console.log(decodedStr.toString('hex'));

//console.log(bufferText.toString('hex'));

process.stdout.write("base64:");

console.log(encodedStr);
//5
