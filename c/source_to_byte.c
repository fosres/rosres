#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc,char**argv)
{
	FILE * in = 0;

	if ( !( in = fopen(argv[1],"rb+") ) )
	{

		fprintf(stderr,"Error: Failed to open file!\n");
	
		exit(1);
	}
	
// Assume array is NON-empty.

	printf("var carr = new Uint8Array([");

	unsigned char c = 0;
	
	fseek(in,0L,SEEK_END);

	unsigned long long int i = 0, size = ftell(in);

	rewind(in);

	while ( (size-i) > 2 )
	{
		c = fgetc(in);

		printf("%u,",c);
	
		i++;
	}
	
	c = fgetc(in);
	
	printf("%u])\n",c);
	
	return 0;
}
