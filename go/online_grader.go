package main

import(
	"io/ioutil"	
	"fmt"
	"net/http"
	"encoding/hex"
	"encoding/base64"
	"unicode"
	"crypto/rand"
	"math/big"
	"crypto/sha256"
	"strings"
	"strconv"
	"os"
	"os/exec"
	"path/filepath"
	"log"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"golang.org/x/crypto/argon2"
)




func dir_size(path string)	(uint64,error)	{

	var size uint64 = 0;

	err := filepath.Walk(path,func(_ string, info os.FileInfo, err error) error	{

		if err != nil	{
		
			return err;
		}

		if !info.IsDir()	{

			size += uint64(info.Size());
		}

		return err;
	});

	return size, err;

}


func enableCors(w * http.ResponseWriter)	{

	(*w).Header().Set("Access-Control-Allow-Origin","*");

}

func write_submission_file(filename string,text * string)		{


	var err error = nil;

	var l int = 0;

	destroy_file(filename);

	f , err := os.Create(string("../submission/" +filename));

	if err != nil			{
		
		log.Fatalln("Error: Failed to create file %s\n",filename,err);

	}

	l, err = f.WriteString(*text);

	if err != nil			{

		log.Fatalln("Error: Failed to write to file %s.\n",filename,err);

	}

	fmt.Println(l,"bytes written successfully");

	err = f.Close();

	if err != nil			{
		
		log.Fatal("Error:Failed to close file\n",err);

		return;
	}
}

func destroy_file(filename string)				{

	
	err := os.Remove(filename);

	if err != nil		{
		
		fmt.Println("Error: Failed to destroy file\n",err.Error());


	}	else		{

	fmt.Println(string("Deleted " + filename + " successfully."));

	}

}

/*
func get_problem(w http.ResponseWriter,r * http.Request)	{
		
		enableCors(&w)

		base64_file_contents, err := ioutil.ReadFile("~/rosres/.c/base64.c")
	
		if err != nil	{

			log.Fatalln("Error: Failed to store file into string",err)
		}	
			
		
		fmt.Fprintf(w,"%s",base64_file_contents);

}
*/

// The following variables must be global in order

// to ensure that the correct output and error

// messages are placed into the online judge.

/*
var execout 	string = "";

var execerr 	string = "";

var errmsg 	string = "";	

var results_table 	string = "";
*/

func addtable(table * string, input string, user_execfile string, judgefile string,execout * string,execerr * string,filetype int,num_fails * uint64,err * error)						{

	var i uint64 = 0;
	
	var judge_execout []byte;

	var cmd * exec.Cmd = nil;

	if filetype <= 3						{
	
		cmd = exec.Command(user_execfile,input);

	} else if filetype == 4						{

		cmd = exec.Command("python",string(user_execfile + ".py"),input);
	
	} else if filetype == 5						{

		cmd = exec.Command("node",string(user_execfile + ".js"),input);

	}

	

	var errs error = nil;
	
	stderr, errs := cmd.StderrPipe();

	*err = errs;
	
	if *err != nil		{

		fmt.Printf("addtable()'s cmd.StderrPipe() failed\n");
	
		*execerr += (*err).Error() + "\n\n";

		*num_fails++;
		
		return;
	}

	if *err = cmd.Start(); *err != nil	{

		fmt.Printf("addtable()'s cmd.Start() failed\n");
		
		*execerr += (*err).Error() + "\n\n";
		
		*num_fails++;
		
		return;
	}
	
	slurp , _ := ioutil.ReadAll(stderr);

	*execerr += string(slurp);

	var output []byte = nil;

	if *err = cmd.Wait(); *err != nil	{

		fmt.Printf("addtable()'s cmd.Wait() failed\n");
		
		*execerr += (*err).Error() + "\n\n";
		
		*num_fails++;

		return;
	}	
	
	
	var execerr_cp = *execerr;
		
	if strings.Contains(execerr_cp,"error:")  == false && strings.Contains(execerr_cp,"# command-line-arguments\n") == false && strings.Contains(execerr_cp,"Error:") == false			{

		if filetype < 4							{	
			
			output, *err = exec.Command(user_execfile,input).Output();

		} else if filetype == 4						{

			output, *err = exec.Command("python",string(user_execfile + ".py"),input).Output();

		} else if filetype == 5						{

			output, *err = exec.Command("node",string(user_execfile + ".js"),input).Output();

		}
		
		if *err != nil		{
		
			fmt.Printf("addtable()'s cmd.Output() for actually executing the file failed!\n");
	
			*execerr += (*err).Error() + "\n\n";
		
			*num_fails++;

			return;
		}
	
		*execout = string(output);
	}
	
	judge_execout, *err = exec.Command(judgefile,input).Output();

	if *err != nil		{
		
		fmt.Println("Judge executable failed to execute");	
	
		*execerr += (*err).Error() + "\n\n";		

		*num_fails++;

		return;
	}

	*table += "<tr>\n";

	if strings.Compare(*execout,string(judge_execout)) == 0		{

		*table += "\t<th>Pass</th>\n";
		
	}	else							{

		*table += "\t<td>Fail</td>\n";

		*num_fails++;
	}

	*table += "\t<td>" + string(input) + "</td>\n";

	*table += "\t<td>";

	var execout_nl string = *execout;

	for ; i < uint64(len(execout_nl)) ; 					{
	
		for ; execout_nl[i] != 0xa ;			{
		
			
			*table += string(execout_nl[i]);
		
			i++;
		}

		*table += "<br>";
			
		i++;
	}	

	*table += "</td>\n";


	*table += "\t<td>";

	i = 0;

	for ; i < uint64(len(judge_execout)) ; 								{
	
		for ; (judge_execout[i] != 0xa) && (i < uint64(len(judge_execout)) ) ;			{
			
			*table += string(judge_execout[i]);
		
			i++;
		}

		*table += "<br>";
			
		i++;
	}	

	*table += "</td>\n";
	
	*table += "</tr>\n";

}

func gentable(user_execout * string,user_execerr * string,user_execfile string,judgefile string,filetype int) (result string)		{

	var input[] byte;

	var num_fails uint64 = 0;

	var err error = nil;

	var table string = "<!DOCTYPE HTML>\n";

	table += "<html>\n";

	table += "<table border =\"1\" bgcolor=\"#ffffff\">\n";

	table += "<tr>\n";

	table += "<th>Pass?</th>\n";

	table += "<th>Input</th>\n";

	table += "<th>Output</th>\n";

	table += "<th>Expected</th>\n";
	
	table += "</tr><br><br>\n";

	var i, j, size uint64 = 0, 0, 0;

	//rand.Seed(time.Now().UnixNano());

	for ; i < 30; 						{
		v, e := rand.Int(rand.Reader,big.NewInt(65));
		
		if e != nil		{

			fmt.Printf("Error: Failed to assign random size using crypto/rand!\n",err.Error());

		}
	
		size = v.Uint64();
	
		input = make([]byte,size);	

		j = 0;

		for ; j < size ;			{
		
			v, e := rand.Int(rand.Reader,big.NewInt(256));

			if e != nil		{

				fmt.Printf("Error: Failed to assign random size using crypto/rand!\n",err.Error());

			}
			
			input[j] = byte(v.Uint64());

			j++;

		}
		
		addtable(&table,base64.StdEncoding.EncodeToString(input[:]),user_execfile,judgefile,user_execout,user_execerr,filetype,&num_fails,&err);

		if err != nil		{

			break;
		}

		i++;	

	}

	table += "</table><br><br>\n";	

	table += "Number of Fails: ";

	table += strconv.FormatUint(num_fails,10);

	table += string("<br><br>\n");
	
	table += "</html>";
	
	return table;
}


func build_progs(sha256_filename * string,text * string,execout * string,execerr * string,filetype int,err * error)		{
	
	fmt.Println("user_file: " + *sha256_filename + "\n");

	switch filetype									{
		

		case 1:							{ //.go files
		
			*sha256_filename = string(*sha256_filename + ".go")
	
			write_submission_file(*sha256_filename,text);

			break;
		}
		
		case 2:							{ // .c files
		
			*sha256_filename = string(*sha256_filename + ".c")

			write_submission_file(*sha256_filename,text);

			break;
		}

		case 3:							{ // .cpp files
		
			*sha256_filename = string(*sha256_filename + ".cpp")
	
			write_submission_file(*sha256_filename,text);

			break;
		}

		case 4:							{ // .py files
		
			*sha256_filename = string(*sha256_filename + ".py")

			write_submission_file(*sha256_filename,text);
			
			break;
		}
		
		case 5:							{ // .js files
		
			*sha256_filename = string(*sha256_filename + ".js")

			write_submission_file(*sha256_filename,text);
			
			break;
		}
	}

	var i 	int   = 0;

	var sha256_filename_cp string = *sha256_filename;
	
	switch filetype									{
		

		case 1:							{ //.go files
			
			i = 0;
	
			for ; (i < len(sha256_filename_cp)) && (sha256_filename_cp[i] != '.'); 	{
				
				i++;
			}	
			
			cmd := exec.Command("go","build","-o",string("../submission/" + sha256_filename_cp[0:i]),string("../submission/" + sha256_filename_cp));

			var errs error = nil;

			stderr, errs := cmd.StderrPipe();

			*err = errs;

			if *err != nil					{
			
				*execerr += (*err).Error() + "\n\n";	
			}

			if *err = cmd.Start(); *err != nil		{
				
				*execerr += (*err).Error() + "\n\n";

	}

			stderrout, _ := ioutil.ReadAll(stderr);
			
			*execerr += string(stderrout);

			if *err = cmd.Wait(); *err != nil		{

				*execerr += (*err).Error() + "\n\n";
			
			}
			
			
			break;
		}
		
		case 2:							{ // .c files
			
			i = 0;
		
			for ; (i < len(sha256_filename_cp)) && (sha256_filename_cp[i] != '.'); 	{
				i++;
			}	

			cmd := exec.Command("gcc",string("../submission/" + sha256_filename_cp),"-o",string("../submission/" + sha256_filename_cp[0:i]),"-lm","-lsodium");
			
			var errs error = nil;

			stderr, errs := cmd.StderrPipe();
		
			*err = errs;
	
			if *err != nil		{
			
				*execerr += (*err).Error() + "\n\n";	
			}

			if *err = cmd.Start(); *err != nil	{
				
				*execerr += (*err).Error() + "\n\n";
	
	}

			stderrout, _ := ioutil.ReadAll(stderr);
			
			*execerr += string(stderrout);

			if *err = cmd.Wait(); *err != nil		{

				*execerr += (*err).Error() + "\n\n";
	
	}
			
			break;
		}

		case 3:							{ // .cpp files
		
			i = 0;
		
			for ; (i < len(sha256_filename_cp)) && (sha256_filename_cp[i] != '.'); 	{
				i++;
			}	

			cmd := exec.Command("g++",string("../submission/" + sha256_filename_cp),"-o",string("../submission/" + sha256_filename_cp[0:i]),"-lm","-lsodium","-fpermissive");

			var errs error = nil;

			stderr, errs := cmd.StderrPipe();
		
			*err = errs;
	
			if *err != nil		{
			
				*execerr += (*err).Error() + "\n\n";	
			}

			if *err = cmd.Start(); *err != nil	{
				
				*execerr += (*err).Error() + "\n\n";

	
	}

			stderrout, _ := ioutil.ReadAll(stderr);
			
			*execerr += string(stderrout);

			if *err = cmd.Wait(); *err != nil		{

				*execerr += (*err).Error() + "\n\n";
	
	}
				
			break;
		}
		
	}
	
}

func only_whitespace(input string) bool					{

	var i uint64 = 0;

	for; i < uint64(len(input)); 				{
		
		if unicode.IsSpace(rune(input[i])) == false	{

			return false;	
		}
	
		i++;
	}

	return true;	
}

func get_form(w http.ResponseWriter,r * http.Request)	{
	
	var execout 	string = "";

	var execerr 	string = "";

	var results_table 	string = "";

	execout = "";

	execerr = "";

	results_table = "";

	//enableCors(&w);

	fmt.Println(r.URL.Path);
	
	var err error = nil;

	err = r.ParseForm()

	if err != nil	{
	
		fmt.Fprintf(w,"Failed to parse form!");
		
		return;
	}

	var sha256_submission string = "";
	
	var sha256_submission_raw string = "";

	var sha256_filename string = "";
	
	var sha256_tablefile string = "";

	r.ParseForm();
	
	sha256_submission_raw = r.PostFormValue("code_submission");

	var language_exp string = r.PostFormValue("language");

	fmt.Printf("language_exp:%s\n",language_exp);

	var i uint64 = 0;

/*
	for ; (i >= 0) && unicode.IsSpace(rune(sha256_submission_raw[i])) ; i-- {}
	
	language_choice = sha256_submission_raw[i:i+1];
*/
	i = 0;

	for ; i < uint64(len(sha256_submission_raw)) ; 				{

		if sha256_submission_raw[i] != 0x0d		{
		
			sha256_submission += string(sha256_submission_raw[i]);

		}

		i++;
	}
	
	var sha256_raw [32]byte = sha256.Sum256([]byte(sha256_submission));	
	
	sha256_filename = hex.EncodeToString(sha256_raw[:]);
	
	sha256_tablefile = string(sha256_filename + ".html");

	fmt.Printf("table_filename: %s\n",sha256_tablefile);

	var filetype int = 0;

	filetype, err = strconv.Atoi(language_exp);

	fmt.Printf("Language Choice:%d\n",filetype);
	
	if err != nil	{

		log.Fatalln("Error: Failed to convert language_choice to 32-bit int\n",err.Error());
	}

	err = nil;

	// execout and execerr declared globally right before addtable() implementation

	build_progs(&sha256_filename,&sha256_submission,&execout,&execerr,filetype,&err);

	// Start building gentable here :D
	
	i = 0;
	
	for ; (i < uint64(len(sha256_filename)) && (sha256_filename[i] != '.'));	{
		
		i++;
	}

	if strings.Contains(execerr,"error:") == false && strings.Contains(execerr,"# command-line-arguments\n") == false									{

		fmt.Printf("Before calling gentable, sha256_filename[0:i]:%s\n",sha256_filename[0:i]);

		results_table = gentable(&execout,&execerr,string("../submission/" + sha256_filename[0:i]),string("../judges/" + r.URL.Path[1:] + "_grader.og"),filetype);	

		write_submission_file(string("../submission/" + sha256_tablefile),&results_table);
	}
	
	
	write_submission_file(string("../submission/" + sha256_filename[0:i] + ".err"),&execerr);

	fmt.Printf("Before creating tablefile %s\n",sha256_tablefile);

	fmt.Printf("sha256_err_filename:%s\n\n",string(sha256_filename[0:i] + ".err"));

	if filetype < 4									{
	
		destroy_file(string("../submission/" + sha256_filename));
		
		destroy_file(string("../submission/" + sha256_filename[0:i]));

	}	else									{

	destroy_file(string("../submission/" + sha256_filename));

	}

}

func verify_hash_salt(w http.ResponseWriter, r * http.Request )						{

	var passwd string = r.PostFormValue("password");

	var salted_hash_bytearr []byte = nil;
	
	var salted_hash string = "";

	var salted_hash_argon [32] byte;

	var err error = nil;

	salted_hash_bytearr, err = base64.StdEncoding.DecodeString(passwd);

	salted_hash = string(salted_hash_bytearr);

	if err != nil {
		
		fmt.Printf("Error! Improperly formatted salted hash!");
	
		return;
	}

	var i, j, k uint64 = 0, 0, 0;

	for ; i < uint64(len(salted_hash)); 				{

		j = i;

		for ; ( j < uint64(len(salted_hash)) ) && ( salted_hash[j] != ',' ) ; 			{
	
			j++;
		}		

		var c_raw uint64 = 0;	
		
		var c uint8 = 0;

		c_raw, err = strconv.ParseUint(salted_hash[i:j],10,8);

		c = uint8(c_raw);

		if err != nil					{
	
			fmt.Printf("Error: Failed to parse salted_hash\n");

			return;

		}

		salted_hash_argon[k] = c;
	
		i = j;
		
		k++;

		i++;
	}

	i = 0;	

	for ; i < 32; 							{

		fmt.Printf("%d ",salted_hash_argon[i]);

		i++;
	}

	fmt.Printf("\n");

}

func write_status_file(filename string,text * string)		{


	var err error = nil;

	var l int = 0;

	destroy_file(filename);

	f , err := os.Create(string("../status/" +filename));

	if err != nil			{
		
		log.Fatalln("Error: Failed to create file %s\n",filename,err);

	}

	l, err = f.WriteString(*text);

	if err != nil			{

		log.Fatalln("Error: Failed to write to file %s.\n",filename,err);

	}

	fmt.Println(l,"bytes written successfully");

	err = f.Close();

	if err != nil			{
		
		log.Fatal("Error:Failed to close file\n",err);

		return;
	}
}

var pwd_dbname string = "";
	

func signup(w http.ResponseWriter,r * http.Request)						{

	fmt.Printf("Made it to signup!\n");
	
	var err error = nil;

	r.ParseForm();

	fmt.Printf("%s\n",r.PostForm);

	var username string = r.PostFormValue("username");
	
	var saltb64 string = r.PostFormValue("salt");

	var ph []byte = nil;

	ph, err = base64.StdEncoding.DecodeString(r.PostFormValue("password"));

	var ph_str string = string(ph);

	if err != nil										{

		fmt.Printf("Failed to decode base64 encoded password hash %s\n",err.Error());
	}
	
	var ph_arr [32]byte;

	var i, j, k uint64 = 0,0,0;

	for ; i < 32;								{

		ph_arr[i] = 0x0;

		i++;
	}

	i = 0;

	j = 0;

	k = 0;

	var ph_byte uint64 = 0;

	for ; i < uint64(len(ph_str)); 								{

		j = i;

		for ; ( j < uint64(len(ph_str)) ) && ( ph_str[j] != ',' )  ;		{

			j++;
		}

		ph_byte, err = strconv.ParseUint(ph_str[i:j],10,8);

		if err != nil						{
			
			fmt.Printf("Error: Failed to parse uint8 byte for password hash for substring: %s\n",ph_str[i:j]);
			
			w.WriteHeader(http.StatusNotAcceptable);

			fmt.Fprintf(w,"Password hash incorrectly formatted. Sorry!");

			return;

		}

		ph_arr[k] = uint8(ph_byte);
	
		i = j;
	
		i++;

		k++;
	}


	fmt.Printf("saltb64: %s\n",saltb64);

	var salt []byte = nil;

	salt, err = base64.StdEncoding.DecodeString(saltb64);

	var salt_str string = string(salt);

	var salt_arr [16]byte;

	var salt_byte uint64 = 0;

	i  = 0;

	for ; i < 16; 						{

		salt_arr[i] = 0x0;

		i++;
	}

	i = 0;
	
	j = 0;

	k = 0;

	for ; i < uint64(len(salt_str)); 							{

		j = i;

		for ; ( j < uint64(len(salt_str) ) ) && salt_str[j] != ',' ;		{

			j++;
		}

		salt_byte, err = strconv.ParseUint(salt_str[i:j],10,8);

		if err != nil								{
		
			fmt.Printf("Error: Failed to parse salt_byte for substring: %s\n",salt_str[i:j]);

			w.WriteHeader(http.StatusNotAcceptable);

			fmt.Fprintf(w,"Salt incorrectly formatted. Sorry!");

			return;

		}


		salt_arr[k] = uint8(salt_byte);

		i = j;

		i++;
		
		k++;	

		
	}	

//	The following is a deliberately CHEAP password hash to provide Server Relief. Parameters are set to the crpyto_pwhash_MIN for both CPU operations and memory

	var ph_prime_raw []byte = argon2.IDKey(ph_arr[:],salt_arr[:],1,8192,4,32);

	var ph_prime_b64 string = base64.StdEncoding.EncodeToString(ph_prime_raw);

	fmt.Printf("ph_prime_b64: %s\n",ph_prime_b64);
		
	database, err := sql.Open("sqlite3",pwd_dbname);

	if err != nil								{

		fmt.Fprintf(os.Stderr,"Failed to open database file!\n");

		w.WriteHeader(http.StatusNotFound);
		
		fmt.Fprintf(w,"Failed to open database file!\n");
			

		return;
	}

	statement, err := database.Prepare("CREATE TABLE IF NOT EXISTS creds (username TEXT UNIQUE,ph TEXT,salt TEXT UNIQUE)");

	if err != nil									{

		fmt.Fprintf(os.Stderr,"Failed to create table!\n");
		
		w.WriteHeader(http.StatusExpectationFailed);
		
		fmt.Fprintf(w,"Failed to create database file!\n");

		return;
	}

	//var text string = "";

	statement.Exec();

	statement, _ = database.Prepare("INSERT INTO creds (username,ph,salt) VALUES (?,?,?)");
	
	
	_, err = statement.Exec(username,ph_prime_b64,saltb64); 		

	//var text string = "";

	if err != nil									{


		
		fmt.Fprintf(os.Stderr,"Error: failed to insert user into database: %s\n",err.Error());

		w.WriteHeader(http.StatusExpectationFailed);

		fmt.Fprintf(w,"Failed to insert username into database!\n");
	

//		write_status_file(username,&text);
	
		return;

	}	

//	text = "Username account creation successful!";

	w.WriteHeader(http.StatusAccepted);

	fmt.Fprintf(w,"Username account creation successful!\n");

//	write_status_file(username,&text);


}


func login(w http.ResponseWriter, r * http.Request)						{

	r.ParseForm();

/*
	var username_hash string = r.PostFormValue("user_hash");

	write_salt_file(username_hash,
*/
}


func main()	{

	if (len(os.Args) < 2)			{
	
		fmt.Fprintf(os.Stderr,"Error: Forgot the credentials database argument!\n");

		return;	
	}
	pwd_dbname = os.Args[1];
	
	fs := http.FileServer(http.Dir("../"));	

	http.HandleFunc("/base64",get_form)

	http.HandleFunc("/login",login);

	http.HandleFunc("/signup",signup);

	fmt.Printf("Starting server at port 8081\n");
	
	var err error = nil;

	http.Handle("/",fs);

	err = http.ListenAndServe(":8081",nil);

	if err != nil		{

		log.Fatal(err);
	}



}
